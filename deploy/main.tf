terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-try2020"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  version = "~> 2.54.0"
  region  = "us-east-1"
}
